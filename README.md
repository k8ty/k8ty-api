k8ty-api
========

This project is the back-end api for https://k8ty.app.

As someone who has worked extensively with Play!, this is a project to get familiar with outer stacks,
and is powered by ZIO and http4s.


## Wonderful Links

* https://github.com/juliano/streaming-all-the-way
* https://medium.com/@pascal.mengelt/what-are-the-benefits-of-the-zio-modules-with-zlayers-3bf6cc064a9b

Firebase Auth Follow Ups
* https://firebase.google.com/docs/admin/setup#initialize-sdk
* https://firebase.google.com/docs/auth/admin/verify-id-tokens#verify_id_tokens_using_the_firebase_admin_sdk
* https://stackoverflow.com/a/41667023
* https://http4s.org/v0.18/auth/